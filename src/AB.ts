export default class AB {
  private readonly a: number;
  private readonly b: number;

  constructor (a: number, b: number) {
    this.a = a
    this.b = b
  }

  plus () {
    return this.a + this.b
  }

  minus () {
    return this.a - this.b
  }

  multiply () {
    return this.a * this.b
  }

  divide () {
    return this.a / this.b
  }

  power () {
    return this.a ** this.b
  }
}
