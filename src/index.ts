import fastify from 'fastify'
import AB from './AB'

const server = fastify({
  logger: true
})

server.get('/', (req, reply) => {
  let { a, b } = req.query as any

  a = parseInt(a)
  b = parseInt(b)

  const ab = new AB(a, b)

  reply.send({
    plus: ab.plus(),
    minus: ab.minus(),
    divide: ab.divide(),
    multiply: ab.multiply(),
    power: ab.power()
  })
})

server.listen(3000, '0.0.0.0').then((address) => {
  console.log(`Server is running on ${address}`)
})
