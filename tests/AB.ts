import AB from '../src/AB'

const expect = require('chai').expect

describe('Test AB class', function () {
  it('AB should calculate a + b correctly', function () {
    const ab = new AB(4, 5)

    expect(ab.plus()).to.be.eq(9)
  })

  it('AB should calculate a - b correctly', function () {
    const ab = new AB(4, 5)

    expect(ab.minus()).to.be.eq(-1)
  })

  it('AB should calculate a * b correctly', function () {
    const ab = new AB(4, 5)

    expect(ab.multiply()).to.be.eq(20)
  })

  it('AB should calculate a / b correctly', function () {
    const ab = new AB(4, 5)

    expect(ab.divide()).to.be.eq(0.8)
  })

  it('AB should calculate a ^  b correctly', function () {
    const ab = new AB(4, 5)

    expect(ab.power()).to.be.eq(1024)
  })
})
